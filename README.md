# Username-Extends Plugin for Connect

**[FUNZIONALITA']**<br />

Questo plugin permette di specificare, lato admin, il numero di volte che un utente può cambiare il proprio username. Questo numero dipenderà dal "tipo di utente" che è l'utente corrente. In particolare, per adesso, le categorie di utenti si dividono in utente normale ed moderatori. La seguente immagine mostra come comparirà la pagina del plugin lato admin:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-username-extends/raw/demaoui/screenshot/connect-username-extends-admin.png)<br />

**[TEST]**

Per testare il plugin basta andare nel profilo di un utente e cambiare il proprio username (dove l'indirizzo sarà http://nameconnect/user/nuov/edit/username):<br />


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-username-extends/raw/demaoui/screenshot/connect-username-extends-user1.png)<br />
<br />
Adesso è possibile cambiare l'username inserendo un altro nome utente (esempio l'username nuovo sarà "nuovo"):<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-username-extends/raw/demaoui/screenshot/connect-username-extends-user2.png)<br />
Un utente può cambiare un numero finito di volte il proprio username e questo numero dipende dal tipo di utente corrente. Una volta che un utente cambia username, bisogna implementare una redirect 301 sugli username che sono stati utilizzati. Nel nostro esempio,se un utente va al link:
http://nameconnect/user/nuov
dove "nuov" è l'username vecchio dell'utente "nuovo", allora ci sarà il redirect sul username corrente di quell'utente.










