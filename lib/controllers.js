"use strict";

var Controllers = {};

const db = require.main.require("./src/database");
const nconf = require.main.require("nconf");
const groups = require.main.require("./src/groups");
const profileController = require.main.require(
  "./src/controllers/accounts/profile"
);

var set_redirect = "connect-username-extends:redirect:";
var hash = "connect-username-extends";

Controllers.renderAdminPage = async (req, res) => {
  const groupsList = await db.getSortedSetRange("groups:createtime", 0, -1);
  var groupNames = groupsList.filter(function (name) {
    return !groups.isPrivilegeGroup(name);
  });
  const groupData = await groups.getGroupsData(groupNames);
  const gruppi_finali = groupData.sort(function (a, b) {
    a = a.name.toLowerCase();
    b = b.name.toLowerCase();
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  });

  const selected_groups = await db.getObject(hash);
  var oggetto_finale = [];
  for (var i = 0; i < gruppi_finali.length; i++) {
    if (
      selected_groups &&
      selected_groups.tipo_utenti &&
      selected_groups.tipo_utenti.length != 0 &&
      isContains(gruppi_finali[i].name, selected_groups.tipo_utenti)
    ) {
      oggetto_finale.push({
        nome_gruppo: gruppi_finali[i].name,
        checked: true,
      });
    } else {
      oggetto_finale.push({
        nome_gruppo: gruppi_finali[i].name,
        checked: false,
      });
    }
  }
  var maxCambi;
  if (selected_groups && selected_groups.maxCambi) {
    maxCambi = selected_groups.maxCambi;
  } else {
    maxCambi = "";
  }
  res.render("admin/plugins/gt-username-extends", {
    gruppi: oggetto_finale,
    maxCambi: maxCambi,
  });
};

function isContains(nome_gruppo, gruppi_cheched) {
  for (var i = 0; i < gruppi_cheched.length; i++) {
    if (gruppi_cheched[i] == nome_gruppo) {
      return true;
    }
  }
  return false;
}

Controllers.redirectURL = async (req, res) => {
  var lowercaseSlug = req.params.userslug.toLowerCase();
  if (req.params.userslug !== lowercaseSlug) {
    if (res.locals.isAPI) {
      req.params.userslug = lowercaseSlug;
    } else {
      return res.redirect(
        nconf.get("relative_path") + "/user/" + lowercaseSlug
      );
    }
  }

  const redirect = await db.getObject(set_redirect + lowercaseSlug);
  if (redirect && redirect.redirect_slug) {
    return res.redirect(
      301,
      nconf.get("relative_path") + "/user/" + userRedirect.redirect_slug
    );
  } else {
    return profileController.get(req, res);
  }
};
module.exports = Controllers;
