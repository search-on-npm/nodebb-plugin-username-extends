"use strict";

var db = require.main.require("./src/database"),
  async = require.main.require("async"),
  privileges = require.main.require("./src/privileges"),
  categories = require.main.require("./src/categories"),
  groups = require.main.require("./src/groups"),
  user = require.main.require("./src/user");
const slugify = require.main.require("./src/slugify");
var Sockets = {};
var hashAdmin = "connect-username-extends";
var set_username = "connect-username-extends:";
var hash_vecchio_username = "connect-username-extends:lista_username_vecchi";

Sockets.setMaxCambi = function (socket, data, callback) {
  var input = {
    tipo_utenti: data.tipo_utenti,
    maxCambi: data.maxCambi,
  };

  db.setObject("connect-username-extends", input, function (err) {
    if (err) {
      return callback(err);
    }
  });
};

Sockets.getNumeroCambiRimanenti = function (socket, data, callback) {
  if (!data) {
    return callback(new Error("Parametri errori"));
  }
  var maxCambi = 0;
  async.waterfall(
    [
      function (next) {
        groups.getUserGroupMembership("groups:createtime", [data.uid], next);
      },
      function (userGroups, next) {
        db.getObject(hashAdmin, function (err, selectedGroups) {
          maxCambi = selectedGroups.maxCambi;
          const found = userGroups[0].some((userGroup) =>
            selectedGroups.tipo_utenti.includes(userGroup)
          );
          next(null, found);
        });
      },
      function (isPermissionFound, next) {
        if (!isPermissionFound) {
          return callback(new Error("Cambio di username non permesso"));
        }
        db.getObject(set_username + "uid:" + data.uid, next);
      },
      function (userChangedUsername, next) {
        var currentlyChangedUsername = 0;
        if (
          userChangedUsername &&
          userChangedUsername.listaUsernameVecchi &&
          userChangedUsername.listaUsernameVecchi.length != 0
        ) {
          var currentlyChangedUsername =
            userChangedUsername.listaUsernameVecchi.length;
        }
        var countrerChanges = maxCambi - currentlyChangedUsername;
        if (countrerChanges <= 0) {
          next(
            new Error("Numero massimo di cambi di username raggiunto"),
            countrerChanges
          );
        } else {
          next(null, countrerChanges);
        }
      },
    ],
    function (err, countrerChanges) {
      if (err) {
        return callback(err);
      }
      return callback(null, countrerChanges);
    }
  );
};

Sockets.getUsernamePrecedenti = function (socket, data, callback) {
  if (!data || !data.uid) {
    return callback(new Error("Parametri errati"));
  }
  async.waterfall(
    [
      function (next) {
        db.getObject(
          set_username + "uid:" + data.uid,
          function (err, username_list) {
            if (err) {
              return next(err);
            }
            if (username_list && username_list.listaUsernameVecchi) {
              return next(null, username_list.listaUsernameVecchi);
            } else {
              return next(null, null);
            }
          }
        );
      },
    ],
    function done(err, result) {
      if (err) {
        return callback(err);
      }
      return callback(null, result);
    }
  );
};

Sockets.isUsernameAlreadyExistis = function (socket, data, callback) {
  if (!data) {
    return callback(new Error("Parametri Errati"));
  }
  var username_input = slugify(data.username_input);
  async.waterfall(
    [
      function (next) {
        db.isSortedSetMember(hash_vecchio_username, username_input, next);
      },
    ],
    function (err, username_utilizzato) {
      if (err) {
        return callback(err);
      }
      return callback(null, username_utilizzato);
    }
  );
};

module.exports = Sockets;
