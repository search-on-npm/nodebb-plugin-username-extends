"use strict";

(function () {
  $(document).ready(function () {
    var elemento_precedente = null;
    $("body").on(
      "keyup",
      "#inputNewUsername",
      debounce(function () {
        var username_input = $(this).val();
        username_input = username_input.trim().toLowerCase();
        var data = {
          username_input: username_input,
        };
        var elemento_corrente = username_input;
        socket.emit(
          "plugins.connectusernameextends.isUsernameAlreadyExistis",
          data,
          function (err, username_usato) {
            if (err) {
              app.alertError(err);
            }
            if (username_usato) {
              $("#inputNewUsername").css("border", "1px solid red");
              $("#inputNewUsername").css(
                "background-color",
                "rgb(250, 255, 189)"
              );

              if (elemento_corrente != elemento_precedente) {
                app.alertError(
                  "Username " + username_input + " è già utilizzato"
                );
                elemento_precedente = elemento_corrente;
              }
            } else {
              $("#inputNewUsername").css("border", "1px solid #ccc");
              $("#inputNewUsername").css("background-color", "");
            }
          }
        );
      }, 500)
    );
  });
})();

//Ritarda il controllo sul testo in input per l'username e sull'email
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}
