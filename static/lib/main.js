"use strict";

define("forum/account/edit/username", ["forum/account/header"], function (
  header
) {
  var AccountEditUsername = {};
  AccountEditUsername.init = function () {
    header.init();

    $("#submitBtn").on("click", function updateUsername() {
      var userData = {
        uid: $("#inputUID").val(),
        username: $("#inputNewUsername").val(),
        password: $("#inputCurrentPassword").val(),
      };

      if (!userData.username) {
        return;
      }

      if (userData.username === userData.password) {
        return app.alertError("[[user:username_same_as_password]]");
      }

      var btn = $(this);
      btn.addClass("disabled").find("i").removeClass("hide");
      socket.emit("user.changeUsernameEmail", userData, function (err, data) {
        btn.removeClass("disabled").find("i").addClass("hide");
        if (err) {
          return app.alertError(err.message);
        }

        var userslug = slugify(userData.username);
        if (
          userData.username &&
          userslug &&
          parseInt(userData.uid, 10) === parseInt(app.user.uid, 10)
        ) {
          $('[component="header/profilelink"]').attr(
            "href",
            config.relative_path + "/user/" + userslug
          );
          $('[component="header/username"]').text(userData.username);
          $('[component="header/usericon"]')
            .css("background-color", data["icon:bgColor"])
            .text(data["icon:text"]);
        }

        ajaxify.go("user/" + userslug);
      });

      return false;
    });
    if ($("#warningconnectusernameextends").length) {
      $("#warningconnectusernameextends").remove();
    }

    socket.emit(
      "plugins.connectusernameextends.getNumeroCambiRimanenti",
      {
        uid: app.user.uid,
      },
      function (err, numero_cambi) {
        if (err) {
          app.alertError(err);
        }
        var stringa = null;
        if (numero_cambi == null) {
          stringa = "L'utente non può cambiare username";
        } else {
          $('[for="inputNewUsername"]').text("Inserisci nuovo username");
          stringa = "Rimangono " + numero_cambi + " cambi";
        }
        $(".edit-form").prepend(
          '<div id="warningconnectusernameextends" class="alert alert-warning">' +
            stringa +
            "</div>"
        );
      }
    );
  };

  return AccountEditUsername;
});
