"use strict";

define("admin/plugins/gt-username-extends", ["settings"], function (Settings) {
  /* globals $, app, socket, require */

  var ACP = {};

  ACP.init = function () {
    Settings.load("gt-username-extends", $(".gt-username-extends-settings"));
    var regex_numero = new RegExp(/^[1-9]+[0-9]*$/);

    $("#save_username").on("click", function () {
      var numero_max = $("#numero_cambio");
      var tipologie_utente = $("#tipologia_utente");
      if (
        numero_max != null &&
        regex_numero.test(numero_max.val().trim()) &&
        tipologie_utente != null
      ) {
        var select_name = [];

        $('input[type="checkbox"]:checked').each(function () {
          select_name.push(
            $(this).attr("id").replace(new RegExp("checkbox_"), "")
          );
        });
        if (!select_name || select_name.length == 0) {
          app.alert({
            type: "danger",
            alert_id: "gt-username-extends-saved",
            title: "ERRORE",
            message: "Seleziona almeno un tipo di utente",
            clickfn: function () {
              socket.emit("admin.reload");
            },
          });
        } else {
          var data = {
            tipo_utenti: select_name,
            maxCambi: numero_max.val().trim(),
          };

          socket.emit(
            "plugins.connectusernameextends.setMaxCambi",
            data,
            function (err) {
              if (err) {
                return err;
              }
            }
          );
          app.alert({
            type: "success",
            alert_id: "gt-username-extends-saved",
            title: "Settings Saved",
            message: "Salvataggio Avvenuto Con Successo",
            clickfn: function () {
              socket.emit("admin.reload");
            },
          });
        }
      } else {
        app.alert({
          type: "danger",
          alert_id: "gt-username-extends-saved",
          title: "ERRORE",
          message: "Inserire il Numero Massimo Cambio Username  ",
          clickfn: function () {
            socket.emit("admin.reload");
          },
        });
      }
    });
  };

  return ACP;
});
