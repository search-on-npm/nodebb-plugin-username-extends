"use strict";
(function () {
  $(window).on("action:ajaxify.end", function (event, data) {
    $(document).ready(function () {
      var profilo = $('div[class="text-center profile-meta"]');
      var creazioneTabella = $("<div>");
      if (profilo) {
        if (ajaxify.data.uid) {
          socket.emit(
            "plugins.connectusernameextends.getUsernamePrecedenti",
            {
              uid: ajaxify.data.uid,
            },
            function (err, username_precedenti) {
              if (err) {
                app.alertError(err.message);
              }

              if (username_precedenti) {
                var htmlAdd = "<br>Username precedenti: ";
                for (var i in username_precedenti) {
                  if (username_precedenti[i] != data.title) {
                    htmlAdd += "<b>" + username_precedenti[i].trim() + "</b>, ";
                  }
                }
                htmlAdd = htmlAdd.replace(/,\s*$/, "");
                profilo.append(htmlAdd);
              }
            }
          );
        }
      }
    });
  });
})();
