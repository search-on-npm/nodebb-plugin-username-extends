<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<p>Il plugin permette, per ogni tipologia di utente, di inserire il numero di volte che tale utente può cambiare username.</p>
			<br />
			<div class="panel-body">
				<form role="form" class="gt-username-extends-settings">
					<div class="form-group row">
						<div class="col-xs-9">
							<div class="col-xs-7">
								<label for="days">Numero max cambio username</label>
							</div>
							<div class="col-xs-2">
								<input type="number" id="numero_cambio" name="days" title="Inserire il numero massimo di volte in cui un utente può cambiare username" class="numero" placeholder="" min="0" value="{maxCambi}"><br />
							</div>
						</div>						
					</div>
				</form>
				<div class="row">
                    <div class="col-xs-10">
                        <div class="col-xs-10">
                            <hr style="width:95%;" />
                        </div>
                    </div>
                                        
                </div>
				<br/>
				<!-- IF gruppi.length -->
				<div class="row">
					<div class="col-xs-9">
						<div class="col-xs-6"><label>Lista Tipi di Utente<label></div>
						<div class="col-xs-3"><label>Seleziona/Deseleziona</label></div>
					</div>
				</div>
				<br/>
				<div class="row">
                    		<div class="col-xs-9">
                        		<div class="col-xs-9">
                            		<hr style="width:90%;" />
                        		</div>
                    		</div>
                                        
                		</div>

					<!-- BEGIN gruppi -->
      					<div class="row">
      							<div class="col-xs-9">
        							<div class="col-xs-6"><label>{gruppi.nome_gruppo}</label></div>
        							<!-- IF gruppi.checked -->
          								<div class="col-xs-3 text-center"><input type="checkbox" id="checkbox_{gruppi.nome_gruppo}" checked ></div>
       								<!-- ELSE -->
         								<div class="col-xs-3 text-center"><input type="checkbox" id="checkbox_{gruppi.nome_gruppo}" ></div>
        							<!-- ENDIF gruppi.checked -->
        					</div> 
      					</div>
      					<div class="row">
                    		<div class="col-xs-9">
                        		<div class="col-xs-9">
                            		<hr style="width:90%;" />
                        		</div>
                    		</div>
                                        
                		</div>
     		 		<!-- END gruppi -->
				<!-- END -->
			</div>
		</div>
	</div>
	<button id="save_username" data-action="create" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
        <i class="material-icons">save</i>
    </button>
</div>