"use strict";

var controllers = require("./lib/controllers");
const db = require.main.require("./src/database");
const sockets = require("./lib/sockets");
const groups = require.main.require("./src/groups");
const SocketPlugin = require.main.require("./src/socket.io/plugins");
const plugin = {};
const routeHelpers = require.main.require("./src/routes/helpers");
const slugify = require.main.require("./src/slugify");

SocketPlugin.connectusernameextends = sockets;

var hashAdmin = "connect-username-extends";
var set_username = "connect-username-extends:";
var set_redirect = "connect-username-extends:redirect:";
var set_lista_vecchi_username =
  "connect-username-extends:lista_username_vecchi";

plugin.init = async (params) => {
  routeHelpers.setupAdminPageRoute(
    params.router,
    "/admin/plugins/gt-username-extends",
    [],
    controllers.renderAdminPage
  );
  routeHelpers.setupPageRoute(
    params.router,
    "/user/:userslug",
    [],
    controllers.redirectURL
  );
};

plugin.usernameCheck = async (data) => {
  if (data.error) {
    throw new Error("[[error:username-error]]");
  }

  const is_used = await db.isSortedSetMember(
    set_lista_vecchi_username,
    data.username
  );
  if (is_used) {
    throw new Error("[[error:username-taken]]");
  }
  return data;
};

plugin.usernameLimitcheck = async (data) => {
  const groupsList = await groups.getUserGroupMembership("groups:createtime", [
    data.uid,
  ]);

  const username_settings = await db.getObject(hashAdmin);
  const found = groupsList[0].some((userGroup) =>
    username_settings.tipo_utenti.includes(userGroup)
  );

  if (!found) {
    throw new Error("[[error:username-change-not-admitted]]");
  }

  const changed_usernames = await db.getObject(
    set_username + "uid:" + data.uid
  );

  var currentlyChangedUsername = 0;
  if (
    changed_usernames &&
    changed_usernames.listaUsernameVecchi &&
    changed_usernames.listaUsernameVecchi.length != 0
  ) {
    currentlyChangedUsername = changed_usernames.listaUsernameVecchi.length;
  }
  var counterChanges = username_settings.maxCambi - currentlyChangedUsername;
  if (counterChanges <= 0) {
    throw new Error("[[error:username-change-limit-reached]]");
  }
  return data;
};

plugin.usernameUpdate = async (data) => {
  let cur_user = await db.getObject(set_username + "uid:" + data.uid);
  var oldUsername = [];

  if (cur_user) {
    oldUsername = cur_user.listaUsernameVecchi;
  }
  oldUsername.push(data.oldData.username);
  await db.setObject(set_username + "uid:" + data.uid, {
    listaUsernameVecchi: oldUsername,
  });

  await db.sortedSetAdd(
    set_lista_vecchi_username,
    Date.now(),
    data.oldData.username
  );
  await db.setObject(set_redirect + slugify(data.oldData.username), {
    redirect_slug: slugify(data.data.username),
  });
};

plugin.addAdminNavigation = async (header) => {
  header.plugins.push({
    route: "/plugins/gt-username-extends",
    icon: "fa-tint",
    name: "Username Extends",
  });

  return header;
};

module.exports = plugin;
